import { Component } from "react";
import Card from "../Card";
import "./style.css";

class Students extends Component {
  render() {
    const { list } = this.props;
    return (
      <div className='container'>
        {list.map((student, index) => (
          <Card student={student} key={index}></Card>
        ))}
      </div>
    );
  }
}

export default Students;
