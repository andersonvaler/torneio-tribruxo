import { Component } from "react";
import "./style.css";

class Card extends Component {
  render() {
    const { name, house, image } = this.props.student;
    return (
      <div className={`${house}`} id='card'>
        <img alt={name} src={image}></img>
        <h3>{name}</h3>
        <p>{house}</p>
      </div>
    );
  }
}

export default Card;
