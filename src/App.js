import { Component } from "react";
import Students from "./components/Students";
import "./App.css";

class App extends Component {
  state = {
    students: [],
    triBruxos: [],
  };

  getBruxos = () => {
    let count = 0;
    let random = [];
    let houses = [];
    while (count < 3) {
      const number = Math.round(Math.random() * (10 - 0) + 0);
      if (
        !random.includes(number) &&
        !houses.includes(this.state.students[number].house)
      ) {
        random.push(number);
        houses.push(this.state.students[number].house);
        count++;
      }
    }
    let bruxos = [];
    for (let i = 0; i < 3; i++) {
      bruxos.push(this.state.students[random[i]]);
    }
    this.setState({
      triBruxos: bruxos,
    });
  };

  componentDidMount() {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => {
        this.setState({ students: response });
      })
      .catch((error) => console.log(error));
  }

  render() {
    return (
      <div className="App">
        {this.state.triBruxos.length > 0 ? (
          <Students list={this.state.triBruxos}></Students>
        ) : (
          <>
            <h1>Torneio Tribruxo</h1>
            <div>Clique no botão para encontrar os feiticeiros!</div>
          </>
        )}

        <button onClick={() => this.getBruxos()}>
          {this.state.triBruxos.length > 0 ? "Tentar Novamente" : "Começar"}
        </button>
      </div>
    );
  }
}

export default App;
